angular.module('ideas.challengeDetail', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('challengeDetail', {
			url: '/challengeDetail',
			views: {
				"main": {
					controller: 'challengeDetailCtrl',
					templateUrl: 'challengeDetail/challengeDetail.tpl.html'
				}
			},
			data: {
				pageTitle: 'Challenge Detail'
			}
		});
	})
	.controller('challengeDetailCtrl', function($scope, $rootScope, $location, $modal ,$stateParams , $state) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});
		$scope.newIdea = function(){			
			$state.go('newIdea') ;
		};
	});