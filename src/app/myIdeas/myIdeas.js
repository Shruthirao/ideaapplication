angular.module('ideas.myIdeas', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('myIdeas', {
			url: '/myIdeas',
			views: {
				"main": {
					controller: 'myIdeasCtrl',
					templateUrl: 'myIdeas/myIdeas.tpl.html'
				}
			},
			data: {
				pageTitle: 'My Ideas'
			}
		});
	})
	.controller('myIdeasCtrl', function($scope, $rootScope, $location, $modal) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});
	});