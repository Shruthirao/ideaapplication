angular.module('ideas.searchIdeas', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('searchIdeas', {
			url: '/searchIdeas',
			views: {
				"main": {
					controller: 'searchIdeasCtrl',
					templateUrl: 'searchIdeas/searchIdeas.tpl.html'
				}
			},
			data: {
				pageTitle: 'Home'
			}
		});
	})
	.controller('searchIdeasCtrl', function($scope, $rootScope, $location, $modal) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});

		$scope.selected = undefined;
		$scope.ideas = ['System Test', 'Desk isuue', 'IP Configuration', 'VoIP',
			'VOIP has to be configured by the NMG.....', 'VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....',
			'System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....'];
	
		$scope.back = function() {
			window.history.back();
		};
	});