angular.module('ideas.trendingChallenges', ['ui.router'])
    .config(function config($stateProvider) {
        $stateProvider.state('trendingChallenges', {
            url: '/trendingChallenges',
            views: {
                "main": {
                    controller: 'trendingChallengesCtrl',
                    templateUrl: 'trendingChallenges/trendingChallenges.tpl.html'
                }
            },
            data: {
                pageTitle: 'Home'
            }
        });
    })
    .controller('trendingChallengesCtrl', function($scope, $rootScope, $location, $modal, $state, $http) {
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

        });




        $scope.trendingDetails = function() {
            $state.go('challengeDetail');
        };

        $scope.IsVisible = false;
        $scope.showSortDropDown = function() {
            $scope.IsVisible = $scope.IsVisible ? false : true;
        };

        /*$scope.postsCtrlAjax = function() {
            $http.get('/trendingChallenges.json').success(function(data) {
                $scope.trendingChallengesJSON = data;
                console.log("Hello");
            });

            console.log("Hi");
        };*/

        //$scope.postsCtrlAjax();

        $scope.trendingChallengesJSON = [

            {
                "id": "01",
                "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                "author": "abc",
                "date": "21 Febuary, 2016",
                "ideasCount": "34"
            },

            {
                "id": "02",
                "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                "author": "xyz",
                "date": "23 February, 2016",
                "ideasCount": "70"
            },

            {
                "id": "03",
                "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                "author": "john",
                "date": "24 February, 2016",
                "ideasCount": "56"
            }
        ];

        $scope.sortByAuthor = function() {

        };

        $scope.sortByCreatedDate = function() {

            $scope.trendingChallengesJSON = [{
                    "id": "03",
                    "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                    "author": "john",
                    "date": "24 February, 2016",
                    "ideasCount": "56"
                },

                {
                    "id": "02",
                    "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                    "author": "xyz",
                    "date": "23 February, 2016",
                    "ideasCount": "70"
                },

                {
                    "id": "01",
                    "description": "HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.",
                    "author": "abc",
                    "date": "21 Febuary, 2016",
                    "ideasCount": "34"
                }
            ];

            $scope.showSortDropDown();
        };

        $scope.sortByIdeasHigh = function() {

        };

        $scope.sortByIdeasLow = function() {

        };

        $scope.sortByNewest = function() {

        };

    });
