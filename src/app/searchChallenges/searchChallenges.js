angular.module('ideas.searchChallenges', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('searchChallenges', {
			url: '/searchChallenges',
			views: {
				"main": {
					controller: 'searchChallengesCtrl',
					templateUrl: 'searchChallenges/searchChallenges.tpl.html'
				}
			},
			data: {
				pageTitle: 'Home'
			}
		});
	})
	.controller('searchChallengesCtrl', function($scope, $rootScope, $location, $modal, $http, commonService) {
		
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		});

		$scope.selected = undefined;
		$scope.challenges = ['System Test', 'Desk isuue', 'IP Configuration', 'VoIP',
			'VOIP has to be configured by the NMG.....', 'VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....','VOIP has to be configured by the NMG.....',
			'System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....','System access denied in the visualization lab....'];
	
		$scope.back = function() {
			window.history.back();
		};

	});

