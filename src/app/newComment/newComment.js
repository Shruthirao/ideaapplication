angular.module('ideas.newComment', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('newComment', {
			url: '/newComment',
			views: {
				"main": {
					controller: 'newCommentCtrl',
					templateUrl: 'newComment/newComment.tpl.html'
				}
			},
			data: {
				pageTitle: 'New Comment'
			}
		});
	})
	.controller('newCommentCtrl', function($scope, $rootScope, $location, $modal) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});
	});
