angular.module('ideas', [
	'ideas.pageHeader',	
	'ideas.landing',
	'ideas.trendingChallenges',
	/*'ideas.trendingChallengesDetail',*/
	'ideas.myChallenges',
	'ideas.challengeDetail',
	'ideas.searchIdeas',
	'ideas.searchChallenges',
	'ideas.newIdea',
	'ideas.myIdeas',
	'ideas.newComment',
	'ideas.newChallenge',
	'ideas.actionSave',

	'ideas.commonService',
	'ideas.exceptionHandler',


	'ui.bootstrap',
	'restangular',
	'templates-app',
	'templates-common',
	'cgBusy',
	'ui.grid'
])

.config(function myAppConfig($stateProvider, $urlRouterProvider, RestangularProvider) {
	$urlRouterProvider.otherwise('/trendingChallenges');
})

.run(function run() {})

.controller('AppCtrl', function AppCtrl($scope, $location, $rootScope, $q) {	
	
	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {		

	});

	$scope.$watch(function() {
		return $rootScope.myPromise;
	}, function(newPromise) {
		$q.when(newPromise).then(null, function(err) {
			//alert('Error when calling service: ' + (err.data ? err.data.message : err.statusText));
			console.error('Error', err);
		});
	});
});