angular.module('ideas.newIdea', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('newIdea', {
			url: '/newIdea',
			views: {
				"main": {
					controller: 'newIdeaCtrl',
					templateUrl: 'newIdea/newIdea.tpl.html'
				}
			},
			data: {
				pageTitle: 'New Idea'
			}
		});
	})
	.controller('newIdeaCtrl', function($scope, $rootScope, $location, $modal) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});
	});