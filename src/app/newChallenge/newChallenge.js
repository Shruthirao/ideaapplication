angular.module('ideas.newChallenge', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('newChallenge', {
			url: '/newChallenge',
			views: {
				"main": {
					controller: 'newChallengeCtrl',
					templateUrl: 'newChallenge/newChallenge.tpl.html'
				}
			},
			data: {
				pageTitle: 'New Challenge'
			}
		});
	})
	.controller('newChallengeCtrl', function($scope, $rootScope, $location, $modal, $state) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {	
			//$scope.categories = [{'name':'Category1'}, {'name':'Category2'}, {'name':'Category2'}]; 
		});

		$scope.postChallenge =  function () {
			var modal = $modal.open({
                templateUrl: 'actionSave/actionSave.tpl.html',
                controller: 'actionSaveCtrl',
                backdrop: 'static',
                windowClass: 'modal-dialog-save'
            });
            modal.result.then(function (result) {
                if (result == 'OK') {
                    $state.go('myChallenges');
                }               
            });
		};
	});