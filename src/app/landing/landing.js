angular.module('ideas.landing', ['ui.router'])
    .config(function config($stateProvider) {
        $stateProvider.state('landing', {
            url: '/landing',
            views: {
                'main': {
                    templateUrl: 'landing/landing.tpl.html'
                }
            },
            data: {
                pageTitle: 'Landing'
            }
        });
    })

;