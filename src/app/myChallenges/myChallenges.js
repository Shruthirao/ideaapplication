angular.module('ideas.myChallenges', ['ui.router'])
	.config(function config($stateProvider) {
		$stateProvider.state('myChallenges', {
			url: '/myChallenges',
			views: {
				"main": {
					controller: 'myChallengesCtrl',
					templateUrl: 'myChallenges/myChallenges.tpl.html'
				}
			},
			data: {
				pageTitle: 'Home'
			}
		});
	})
	.controller('myChallengesCtrl', function($scope, $rootScope, $location, $modal) {
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			
		});
	});