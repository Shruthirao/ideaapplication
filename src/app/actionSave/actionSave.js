angular.module('ideas.actionSave', [])
    .controller('actionSaveCtrl', function ($scope, $modalInstance) {
        $scope.close = function () {
            $modalInstance.dismiss();
        };
        $scope.save = function () {
            $modalInstance.close('OK');
        };
});
