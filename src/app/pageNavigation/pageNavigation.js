angular.module( 'ideas.pageHeader', ['ngSanitize'])
.directive('pageNavigation', function() {
	return {
		replace: 'true',
		restrict: 'E',		
		scope: {
			appName: '=',
			userName: '=',
			currentTab:'='			
		},
		templateUrl: 'pageNavigation/pageNavigation.tpl.html',		
		
		controller: function($scope) {
			/*$scope.status = {
				isopen: false
			};*/

			/*console.log($scope.currentTab);*/
		}
	};
});
